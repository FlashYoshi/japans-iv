# Grammar

## Chapter 12

- [Grammar](#grammar)
	- [Chapter 12](#chapter-12)
		- [**Looks like it is -y**](#looks-like-it-is--y)
		- [**Reason/cause through て-form**](#reasoncause-through-て-form)
		- [**Comparing - better of two**](#comparing---better-of-two)
		- [**Comparing - not as**](#comparing---not-as)
		- [**Comparing - best of list**](#comparing---best-of-list)
	- [Chapter 13](#chapter-13)
		- [**Having experienced something**](#having-experienced-something)
		- [**Too much**](#too-much)
		- [**Contrast using が**](#contrast-using-が)
		- [**もう・まだ**](#もうまだ)
	- [Chapter 14](#chapter-14)
		- [**Expressing passed time**](#expressing-passed-time)
		- [**Asking permission**](#asking-permission)
		- [**Informal negative form (ない)**](#informal-negative-form-ない)
		- [**Have to**](#have-to)
	- [Chapter 15](#chapter-15)
		- [**Expressing precedence**](#expressing-precedence)
		- [**Light suggestion**](#light-suggestion)
		- [**Please don't**](#please-dont)
		- [**A after B**](#a-after-b)

### **Looks like it is -y**

> イA/ナA + そうです  
> イA/ナA + そうな N

A supposition based on the appearance of a thing, person, etc.

> |                    | 前                | 後                       |
> |--------------------|-------------------|--------------------------|
> |                    |                   | 美味しいそうです         |
> | イA + そうです     | 美味しい          | 美味しいそうでした       |
> |                    |                   | 美味しくな**さ**そうです |
> |                    | いい              | 良**さ**そうです         |
> | ナA + そうです     | 便利な            | 便利そうです             |
> |                    |                   | 暇じゃな**さ**そうです     |
> | イA/ナA + そうな N | 美味しい + お菓子 | 美味しそうなお菓子       |
> | V + そうです       | 雨が降ります      | 雨が降りそうです         |

* 子供達は楽しそうですね。
* 猫は気持ちが良さそうですね。
* 猫の横に暇そうな男の人がいます。
* 美味しいそうなサラダですね。

### **Reason/cause through て-form**

> イA + くて/なくて  
> ナA + で/じゃなくて

`て` can be used to express reason or cause.

* 今日はとても暑いです。このレモンジュースは冷たくて、とても美味しいです。
* 私は辛い物が苦手です。このスーパは辛くなくて、美味しいです。
* このサラダは野菜が新鮮で、美味しいです。
* ピクニックの食べ物は好きな料理が少なくて、残念でした。

### **Comparing - better of two**

> N1と N2と どちら（の方）が A ですか。  
> N1/N2（の方）が A です。

Between `N1` and `N2`, which one is more `A`?

* お茶とコーヒーとどちらの方が美味しいですか。お茶の方が美味しいです。
* 果物と野菜とどちらが好きですか。果物が好きです。
* 猿と象とどちらがかっこいですか。象の方がかっこいです。

### **Comparing - not as**

> N1（の方）が N2より Aです。  
> N1は N2ほど A ない です。  
> N2は N1ほど A ない です。

Is `N1` as `A` as `N2`? No/yes, `N1`/`N2` is not as `A` as `N1`/`N2`.

* お茶とコーヒーより美味しいですか。コーヒーはお茶ほど美味しくないです。
* 果物と野菜より好きですか。野菜は果物より好きじゃないです。
* 猿と象よりかっこいですか。猿は像ほどかっこくないです。

### **Comparing - best of list**

> N1と N2と N3の 中では、どれが いちばん A ですか。  
> N1と N2と N3の 中では、N1/N2/N3が いちばん A です。

Which one is the most `A` between `N1`, `N2`, and `N3`?

* 日本料理の中では、どれが一番好きですか。しゃぶしゃぶが一番好きです。
* 肉と魚と卵の中では、どれが一番好きですか。肉が一番好きです。
* シンさんとナカムラさんとヤンさんの中では、誰が一番背が高いですか。ヤンさんは一番背が高いです。

## Chapter 13

### **Having experienced something**

> Vた ことが あります

Stating whether someone has ever experienced doing `V`.

* ナカムラさんはシドニー支社に行ったことがありますか。
* シンさんは今までに通訳をしたことがありますか。
* 外国に出張したことがありますか。

### **Too much**

> イA/ナA/V すぎます

The speaker thinks that something expressed by `イA`/`ナA`/`V` is excessive and thus not very good.

> |     |          | 前       | 後           |
> |-----|----------|----------|--------------|
> | イA |          | 暗い     | 暗すぎます   |
> | ナA | すぎます | 静かな   | 静かすぎます |
> | V   |          | 食べます | 食べすぎます |

* この部屋は寒すぎます。
* この部屋は静かすぎます。ちょっと怖いです。
* 昨日の夜、飲みすぎました。今日は早く帰ります。

### **Contrast using が**

> S1 が、S2

`が` indicates a contrast between `S1` and `S2`.

* この部屋はちょっと暗いですが、大丈夫です。
* エアコンがありますが、壊れています。
* シャワーのお湯は出ますが、ぬるいです。

### **もう・まだ**

> |      |              | 翻訳                         | 前         | 後                   |
> |------|--------------|------------------------------|------------|----------------------|
> | もう | 〜ました     | Did you ~ yet? / I already ~ | あげます   | もうあげました       |
> |      | 〜ません     | I do not ~ anymore           | 食べます   | もう食べません       |
> | まだ | 〜ています   | Are you still ~ing?          | 飲みます   | まだ飲みています     |
> |      | 〜ていません | I did not yet ~              | 終わります | まだ終わっていません |
> |      | 〜ます       | Do you still ~?              | 登る       | まだ登ります         |

* もう晩御飯を食べましたか。はい、もう食べました。
* いいえ、まだ食べていません。
* もう新しい本を読みましたか。いいえ、まだ読んでいません。
* まだ食べていますか。いいえ、もう食べません。
* まだ登りますか。はい、時々登ります。

## Chapter 14

### **Expressing passed time**

> V-て 〜年 / 〜ヶ月に 成ります

Expressing how much time has passed since the action state began.

* 十年前にこの会社に入りました。-> この会社に入って十年に成ります。
* 三ヶ月前にこのレストランを始めました。-> このレストランを始めて三ヶ月に成ります。
* 四年前に東京に来ました。-> 東京に来て、四年に成ります。

### **Asking permission**

> V-ても いいですか

`て-form + も いいですか` can be used to ask permission to do something.

* 暗いですね。電気をつけてもいいですか。
* じゃあ、帰りましょう。電気を消してもいいですか。
* あのう、これ、コピーしてもいいですか。

### **Informal negative form (ない)**

> | 動詞型     | 最高の文字 | 活用     | 前     | 後         |
> |------------|------------|----------|--------|------------|
> |            | 〜う       | 〜わない | 会う   | 合わない   |
> |            | 〜つ       | 〜たない | 持つ   | 持たない   |
> |            | 〜る       | 〜らない | 帰る   | 帰らなない |
> |            | 〜き       | 〜かない | 書く   | 書かない   |
> | 五段       | 〜ぐ       | 〜がない | 泳ぐ   | 泳がない   |
> |            | 〜す       | 〜さない | 話す   | 話さない   |
> |            | 〜む       | 〜まない | 飲む   | 飲まない   |
> |            | 〜ぶ       | 〜ばない | 遊ぶ   | 遊ばない   |
> |            | 〜ぬ       | 〜なない | 死ぬ   | 死ない     |
> | 一段       | 〜る       | 〜ない   | 食べる | 食べない   |
> | 不規則動詞 |            |          | する   | しない     |
> |            |            |          | くる   | こない     |

### **Have to**

> V-ない なければ なりません

You must do `V`.

* 明日の朝、６時に起きなければなりません。
* 毎日、練習しなければなりません。
* 薬を飲みなければなりません。

## Chapter 15

### **Expressing precedence**

> V1-る 前に、 V2

Used to express that `V2` happened before `V1`.

* 走る前に、体操をします。
* 寝る前に、お風呂に入ります。
* 泳ぐ前に、体操をします。

### **Light suggestion**

> V-ると いいです(よ)

`と いいです` can be used to make light suggestions. Often `よ` is used at the end of such a sentence.

* 毎日コンピューターで仕事して、肩が痛いです。肩をゆっくり回すといいですよ。
* 最近、目が疲れています。ブルーベリーを食べるといいですよ。
* 今日疲れます。寝るといいですよ。

### **Please don't**

> V-ないで 下さい

Used when warning or advising someone not to do something.

* プールのそばに走らないで下さい。
* あの花は中毒です。食べないで下さい。
* この章は重要ではありません。勉強しないで下さい。

### **A after B**

> V1た 後で、V2  
> Nの 後で、V2

Used when talking about doing `V2` after `V1`/`N`.

* 山田さんは日本に帰った後で、病気に成りました。
* 私は授業の後で、図書館に行きました。
* 勉強した後で、食べます。
